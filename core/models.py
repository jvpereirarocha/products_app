from django.db import models

class Produto(models.Model):
    descricao = models.CharField(verbose_name='Descrição', max_length=100)
    preco = models.DecimalField(verbose_name='Preço', max_digits=10,\
                                decimal_places=2)
    quantidade = models.IntegerField(verbose_name='Quantidade em Estoque')

    def __str__(self):
        return self.descricao

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'


class Cliente(models.Model):
    nome = models.CharField(verbose_name='Nome', max_length=50)
    sobrenome = models.CharField(verbose_name='Sobrenome', max_length=100)
    email = models.EmailField(verbose_name='E-mail', max_length=255)

    def __str__(self):
        return f'{self.nome} {self.sobrenome}'

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
