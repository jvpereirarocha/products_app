from django.urls import path

from .views import index, contato, detalhes

urlpatterns = [
    path('', index, name='index'),
    path('contato', contato, name='contato'),
    path('produto/<int:pk>', detalhes, name='produto_detalhe'),
]
