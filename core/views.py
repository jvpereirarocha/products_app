from django.shortcuts import render, get_object_or_404, redirect
from .models import Produto, Cliente
from django.http import HttpResponse
from django.template import loader

def index(request):
    produtos = Produto.objects.all()
    mensagem = "Lista de produtos"
    context = {
        'message': mensagem,
        'products': produtos,
    }
    return render(request, 'index.html', context)


def contato(request):
    return render(request, 'contato.html')


def detalhes(request, pk):
    produto = get_object_or_404(Produto, pk=pk)
    context = {}
    context['produto'] = produto
    return render(request, 'details.html', context)


def not_found_page(request, ex):
    template = loader.get_template('page_404.html')
    return HttpResponse(content=template.render(),\
                        content_type='text/html; charset=utf8', status=404)


def error_505(request):
    template = loader.get_template('page_500.html')
    return HttpResponse(content=template.render(),\
                        content_type='text/html; charset=utf8', status=500)
